"""
..
  Copyright 2022 Jan P Buchmann

.. moduleauthor:: Jan Piotr Buchmann <jpb@members.fsf.org>
"""

import os
import re
import sys
import subprocess
import timeit
import threading

import guppy.resultparser
import nvidiasmi.monitor

class Guppy:

  params = ['gpu_runners_per_device','chunk_size','chunks_per_runner',
            'chunks_per_caller', 'input_path', 'save_path', 'device', 'config']
  re_guppyver = re.compile('.+Version\D+(\d+.+)[,|\s]*')
  re_mmapver = re.compile('.+minimap2\D+(\d+.+?)\s*')

  @staticmethod
  def guppy_version(path):
    p = subprocess.Popen([path, '--version'], text=True, stdout=subprocess.PIPE)
    p.wait()
    versions = {'guppyver':None, 'mmapver':None}
    for i in p.stdout:
      if Guppy.re_guppyver.search(i):
        versions = {'guppyver' : Guppy.re_guppyver.match(i).groups(1)[0]}
      if Guppy.re_mmapver.match(i):
        versions = {'mmapver' : Guppy.re_guppyver.match(i).groups(1)[0]}
    p.stdout.close()
    return versions

  class Stdout:

    def __init__(self, path) -> None:
      self.path = path
      self.fh = None

    def open(self):
      self.fh = open(self.path, 'w+')
      return self.fh

    def rewind(self):
      self.fh.seek(0)

    def close(self):
      self.fh.close()

  class Stderr:

    def __init__(self, path) -> None:
      self.path = path
      self.fh = None

    def open(self):
      self.fh = open(self.path, 'w+')
      return self.fh

    def close(self):
      self.fh.close()

  def __init__(self, ruid, path, version, config, memory_available, benchmark_basedir) -> None:
    self.ruid = ruid
    self.path = path
    self.version = version
    self.config = config.name()
    self.model_factor = config.model_factor()
    self.command = [self.path, '--disable_pings', '--trim_strategy', 'dna']
    self.benchmark_dir = os.path.join(benchmark_basedir, 'benchmark-'+ruid)
    self.settings = {'config':config.path(), 'save_path':os.path.join(self.benchmark_dir, 'guppy-run')}
    self.memory_available = memory_available
    self.gpu_indices = None
    self.duration = None
    self.stdout = Guppy.Stdout(os.path.join(self.benchmark_dir, 'guppy-'+self.ruid+'.stdout'))
    self.stderr = Guppy.Stderr(os.path.join(self.benchmark_dir, 'guppy-'+self.ruid+'.stderr'))

  def guppy(self):
    return self.path

  def set_gpu_runners_per_device(self, gpu_runners_per_device):
    self.settings['gpu_runners_per_device'] = int(gpu_runners_per_device)

  def set_chunk_size(self, chunk_size):
    self.settings['chunk_size'] = int(chunk_size)

  def set_chunks_per_runner(self, chunks_per_runner):
    self.settings['chunks_per_runner'] = int(chunks_per_runner)

  def set_chunks_per_caller(self, chunks_per_caller):
    self.settings['chunks_per_caller'] = int(chunks_per_caller)

  def set_recursive(self):
    self.settings['recursive'] = None

  def set_input_path(self, path):
    self.settings['input_path'] = path

  def set_save_path(self, path):
    self.settings['save_path'] = path

  def set_gpu_device(self, gpu_indices):
    self.gpu_indices = gpu_indices
    self.settings['device'] =  ' '.join(x for x in ['cuda:'+str(x) for x in gpu_indices])

  def set_config(self, config):
    self.settings['config'] = config

  def assemble_command(self):
    command = [] + self.command
    for i in self.settings:
      command.append('--'+i)
      if self.settings[i] is not None:
        command.append(str(self.settings[i]))
    return command

  def run(self):
    command = self.assemble_command()
    start = timeit.default_timer()
    p = subprocess.Popen(command, stdout=self.stdout.open(), stderr=self.stderr.open())
    gpumon = nvidiasmi.monitor.GpuMonitor(self.memory_available, self.benchmark_dir, self.ruid, p.pid, self.gpu_indices)
    thread = threading.Thread(name='gpumon', target=gpumon.start)
    thread.start()
    p.wait()
    duration = timeit.default_timer() - start
    gpumon.stop()
    thread.join()
    results = self.collect_results(p.returncode, duration)
    self.stdout.close()
    self.stderr.close()
    if p.returncode  == 0:
      os.remove(self.stderr.path)
    return results

  def chunk_size(self):
    return self.settings['chunk_size']

  def gpu_runners_per_device(self):
    return self.settings['gpu_runners_per_device']

  def chunks_per_runner(self):
    return self.settings['chunks_per_runner']

  def chunks_per_caller(self):
    return self.settings['chunks_per_caller']

  def required_memory(self):
    return self.gpu_runners_per_device() * self.model_factor * self.chunk_size() * self.chunks_per_runner()

  def show_command(self):
    print(self.assemble_command())

  def save_path(self):
    return self.settings.get('save_path')

  def collect_results(self, exit_code, duration):
    results = {'exitCode':exit_code, 'duration':duration}
    self.stdout.rewind()
    grp = guppy.resultparser.GuppyResultParser()
    results.update(grp.parse_stdout(self.stdout.fh))
    return results

  def run_parameters(self):
    parameters = {**self.settings}
    parameters['config'] = self.config
    parameters.update({'ruid':self.ruid, 'path':self.path, 'version':self.version})
    return parameters
