"""
..
  Copyright 2022 Jan P Buchmann

.. moduleauthor:: Jan Piotr Buchmann <jpb@members.fsf.org>
"""

import os

class GuppyConfig:

  modelfactors = {'fast':1200, 'hac':3300, 'sup':12600}
  suffix = '.cfg'

  def __init__(self) -> None:
    self.locpath = None
    self.descr = None
    self.model_type = None

  def show(self):
    print(self.locpath, self.descr, self.modeltype, self.modelfactor)

  def configure(self, cfg_path):
    if cfg_path.endswith(GuppyConfig.suffix):
      descr = os.path.split(cfg_path)[1]
      for i in descr[:-len(GuppyConfig.suffix)].split('_'):
        if i in GuppyConfig.modelfactors:
          self.locpath = cfg_path
          self.descr = descr
          self.model_type = i
    return self

  def model_factor(self):
    return GuppyConfig.modelfactors.get(self.model_type)

  def path(self):
    return self.locpath

  def name(self):
    return self.descr
