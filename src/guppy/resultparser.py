"""
..
  Copyright 2022 Jan P Buchmann

.. moduleauthor:: Jan Piotr Buchmann <jpb@members.fsf.org>
"""

import re
import sys

class GuppyResultParser:

  scinot_pattern = '[-+]?[\d]+\.?[\d]*[Ee](?:[-+]?[\d]+)?'
  re_qscore = re.compile('^minimum qscore:\D+(\d+)')
  re_chunk_size = re.compile('^chunk size\D+(\d+)')
  re_chunks_per_runner = re.compile('^chunks per runner\D+(\d+)')
  re_recnum = re.compile('^records per file:\D+(\d+)')
  re_numbasecaller = re.compile('^num basecallers:\D+(\d+)')
  re_runners_per_device = re.compile('^runners\s+per\D+(\d+)')
  re_gpu_device = re.compile('^gpu_device:\s*(.+)')
  re_filenum = re.compile('^Found\D+(\d+)\s+fast5\s.+')
  re_samplesstat = re.compile(f"^Caller time\D+(\d+)\D+(\d+)\D+({scinot_pattern}|(\d+))$")

  result_columns = ['minQscoreRes', 'chunkSizeRes', 'chunksPerRunnerRes',
                    'recordsPerFile', 'numBasecallersRes', 'runnerPerDeviceRes',
                    'gpuDeviceRes', 'numFilesRes', 'numSamplesRes', 'samplesSecondRes',
                    'time', 'exitCode', 'duration']

  def __init__(self) -> None:
    pass

  def parse_stdout(self, stdout_fh):
    stats = {}
    for i in stdout_fh:
      if GuppyResultParser.re_qscore.match(i):
        stats['minQscoreRes'] =int(GuppyResultParser.re_qscore.match(i).groups(1)[0])
      if GuppyResultParser.re_chunk_size.match(i):
        stats['chunkSizeRes'] = int(GuppyResultParser.re_chunk_size.match(i).groups(1)[0])
      if GuppyResultParser.re_chunks_per_runner.match(i):
        stats['chunksPerRunnerRes'] = int(GuppyResultParser.re_chunks_per_runner.match(i).groups(1)[0])
      if GuppyResultParser.re_recnum.match(i):
        stats['recordsPerFile'] = int(GuppyResultParser.re_recnum.match(i).groups(1)[0])
      if GuppyResultParser.re_numbasecaller.match(i):
        stats['numBasecallersRes'] = int(GuppyResultParser.re_numbasecaller.match(i).groups(1)[0])
      if GuppyResultParser.re_runners_per_device.match(i):
        stats['runnerPerDeviceRes'] = int(GuppyResultParser.re_runners_per_device.match(i).groups(1)[0])
      if GuppyResultParser.re_gpu_device.match(i):
        stats['gpuDeviceRes'] = int(GuppyResultParser.re_gpu_device.match(i).groups(1)[0])
      if GuppyResultParser.re_filenum.match(i):
        stats['numFilesRes'] = int(GuppyResultParser.re_filenum.match(i).groups(1)[0])
      if GuppyResultParser.re_samplesstat.match(i):
        stats['time'] = int(GuppyResultParser.re_samplesstat.match(i).groups(1)[0])
        stats['numSamplesRes'] = int(GuppyResultParser.re_samplesstat.match(i).groups(1)[1])
        stats['samplesSecondRes'] = float(GuppyResultParser.re_samplesstat.match(i).groups(1)[2])
    # if len(stats) != 11:
    #   print(f"Warning: Error parsing guppy stdout", file=sys.stderr)
    return stats

  def test_parsing(self, stdoutfile):
    print(stdoutfile)
    fh = open(stdoutfile, 'r')
    res = self.parse_stdout(fh)
    print(res)
    fh.close()
    for i in res:
      print(i, res[i])
