"""
..
  Copyright 2022 Jan P Buchmann

.. moduleauthor:: Jan Piotr Buchmann <jpb@members.fsf.org>
"""

import os
import time

import nvidiasmi.gpuquery
import nvidiasmi.computeappquery

class GpuMonitor:

  class GpuReport:
    header = ['time[UTC-epoch]', 'runUuid', 'gpuUuid', 'gpuIdx', 'gpuName',
              'gpuDriverVer', 'gpuTemp', 'gpuUtil[%]', 'memUtil[%]',
              'mem_used[Mib]', 'memAvailable[Mib]', 'memFree[Mib]',
              'memTot[Mib]', 'pid', 'pidName', 'pidMemUse[Mib]']

    def __init__(self, path) -> None:
      self.path = path
      self.fh = None

    def write_header(self):
      self.fh.write('#'+"\t".join(GpuMonitor.GpuReport.header)+"\n")

    def open(self):
      self.fh = open(self.path, 'w')
      return self.fh

    def close(self):
      self.fh.close()

    def write_status(self, statuscols):
      self.fh.write("\t".join([str(x) for x in statuscols])+"\n")

  class GpuStatus:

    def __init__(self, idx=-1, uuid=None, name=None, driver_ver=None, temp=0,
                 util_gpu=-1, util_mem=-1, mem_used=-1, mem_free=-1, mem_tot=0) -> None:
      self.idx = int(idx)
      self.uuid = uuid
      self.name = name
      self.driver_ver = driver_ver
      self.temp = float(temp)
      self.util_gpu = float(util_gpu)
      self.util_mem = float(util_mem)
      self.mem_used = float(mem_used)
      self.mem_free = float(mem_free)
      self.mem_tot = float(mem_tot)

  class AppStatus:

    def __init__(self, pid=-1, name=None, gpu_uuid=None, mem_used=-1) -> None:
      self.pid = int(pid)
      self.name = name
      self.gpu_uuid = gpu_uuid
      self.mem_used = float(mem_used)

  def __init__(self, memory_available, benchmark_dir, run_uuid, pid, gpu_indices, sleep=1) -> None:
    self.memory_available = memory_available
    self.report = GpuMonitor.GpuReport(os.path.join(benchmark_dir, 'gpu-stats.tsv'))
    self.ruid = run_uuid
    self.sleep = sleep
    self.gpuqry = nvidiasmi.gpuquery.GpuQuery()
    self.appqry = nvidiasmi.computeappquery.ComputeAppQuery()
    self.do_stop = False
    self.pid = pid
    self.gpu_indices = gpu_indices

  def start(self):
    self.report.open()
    self.report.write_header()
    while not self.do_stop:
      gpu_stats = self.query_gpu_stats()
      app_stats = self.query_app_stats()
      if not app_stats:
        t = time.time()
        for i in gpu_stats:
          self.report.write_status([t, self.ruid, gpu_stats[i].uuid,
            gpu_stats[i].idx, gpu_stats[i].name, gpu_stats[i].driver_ver,
            gpu_stats[i].temp, gpu_stats[i].util_gpu, gpu_stats[i].util_mem,
            gpu_stats[i].mem_used, self.memory_available, gpu_stats[i].mem_free,
            gpu_stats[i].mem_tot, 'none', 'none', 'none'])
      for i in app_stats:
        if i == int(self.pid) and app_stats[i].gpu_uuid in gpu_stats:
          self.report.write_status([time.time(), self.ruid,
            gpu_stats[app_stats[i].gpu_uuid].uuid, gpu_stats[app_stats[i].gpu_uuid].idx,
            gpu_stats[app_stats[i].gpu_uuid].name, gpu_stats[app_stats[i].gpu_uuid].driver_ver,
            gpu_stats[app_stats[i].gpu_uuid].temp, gpu_stats[app_stats[i].gpu_uuid].util_gpu,
            gpu_stats[app_stats[i].gpu_uuid].util_mem, gpu_stats[app_stats[i].gpu_uuid].mem_used,
            self.memory_available, gpu_stats[app_stats[i].gpu_uuid].mem_free,
            gpu_stats[app_stats[i].gpu_uuid].mem_tot, app_stats[i].pid,
            app_stats[i].name, app_stats[i].mem_used])
      time.sleep(self.sleep)
    self.report.close()

  def stop(self):
    self.do_stop = True

  def query_gpu_stats(self):
    query=['index', 'uuid', 'name', 'driver_version', 'temperature.gpu',
           'utilization.gpu', 'utilization.memory', 'memory.used',
           'memory.free', 'memory.total']
    gpu_stats = {}
    for i in self.gpu_indices:
      gpuqry = self.gpuqry.query(query, gpu_index=i)
      for i in gpuqry.stdout:
        cols = i.strip().split(',')
        if cols[1] not in gpu_stats:
          gpu_stats[cols[1]] = {}
        gpu_stats[cols[1]] = GpuMonitor.GpuStatus(cols[0], cols[1], cols[2],
          cols[3], cols[4], cols[5], cols[6], cols[7], cols[8], cols[9])
    return gpu_stats

  def query_app_stats(self):
    query=['pid', 'name', 'gpu_uuid', 'used_memory']
    app_stats = {}
    appqry = self.appqry.query(query)
    for i in appqry.stdout:
      cols = i.strip().split(',')
      if int(cols[0]) == self.pid:
        app_stats[int(cols[0])] = GpuMonitor.AppStatus(cols[0], cols[1], cols[2], cols[3])
    return app_stats
