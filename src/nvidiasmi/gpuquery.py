"""
..
  Copyright 2022 Jan P Buchmann

.. moduleauthor:: Jan Piotr Buchmann <jpb@members.fsf.org>
"""

import nvidiasmi.nvidiasmi


class GpuQuery(nvidiasmi.nvidiasmi.NvidiaSmi):
# vbios_version
  header = ['idx', 'uuid', 'name', 'driverVersion', 'memTotal[Mib]', 'memFree[Mib]']

  class Gpu:
    def __init__(self, index, uuid, name, driver_ver, mem_total, mem_free) -> None:
      self.index = index
      self.uuid = uuid
      self.name = name
      self.driver_version = driver_ver
      self.mem_total = mem_total
      self.mem_free = mem_free

    def dump(self):
      return [str(x) for x in [self.index, self.uuid, self.name,
              self.driver_version,
              GpuQuery.bytes_to_mib(self.mem_total),
              GpuQuery.bytes_to_mib(self.mem_free)]]

    def free_memory(self):
      return self.mem_free

    def total_memory(self):
      return self.mem_total

    def attributes(self):
      return {'index':self.index, 'uuid':self.uuid, 'name':self.name, 'driverVersion':self.driver_version,
              'memTotal': self.mem_total, 'memFree':self.mem_free}

  def __init__(self) -> None:
    super().__init__()
    self.query_switch = 'query-gpu'

  def query_gpus(self):
    gpus = {}
    nv_smi = self.query(['index', 'uuid', 'name', 'driver_version', 'memory.total', 'memory.free'])
    for i in nv_smi.stdout:
      cols = i.strip().split(',')
      gpus[cols[1].strip()] = GpuQuery.Gpu(int(cols[0]), cols[1].strip(), cols[2], cols[3],
                                           GpuQuery.mib_to_bytes(int(cols[4])), GpuQuery.mib_to_bytes(int(cols[5])))
    nv_smi.stdout.close()
    return gpus

  def list_gpus(self):
    print(self.ors.join(GpuQuery.header))
    gpus = self.query_gpus()
    for i in gpus:
      print(self.ors.join(str(x) for x in gpus[i].dump()))
