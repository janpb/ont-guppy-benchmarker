"""
..
  Copyright 2022 Jan P Buchmann

.. moduleauthor:: Jan Piotr Buchmann <jpb@members.fsf.org>
"""

import subprocess

class NvidiaSmi:

  @staticmethod
  def bytes_to_mib(bytes):
    return bytes / 2**20

  @staticmethod
  def mib_to_bytes(mib):
    return mib * 2**20

  def __init__(self) -> None:
    self.cmd = ['nvidia-smi']
    self.format = ['--format=csv,noheader,nounits']
    self.query_switch = None
    self.ors = "\t"

  def query(self, query, format=None, gpu_index=None):
    if not format:
      format = self.format
    cmd = self.cmd + [''.join(['--'+self.query_switch+'='] +
                     [','.join(str(x) for x in query)])] + self.format
    if gpu_index is not None:
      cmd + ['--id='+str(gpu_index)]
    proc = subprocess.Popen(cmd, text=True, stdout=subprocess.PIPE)
    proc.wait()
    return proc
