"""
..
  Copyright 2022 Jan P Buchmann

.. moduleauthor:: Jan Piotr Buchmann <jpb@members.fsf.org>
"""

from xml.dom.minidom import AttributeList
import nvidiasmi.nvidiasmi


class ComputeAppQuery(nvidiasmi.nvidiasmi.NvidiaSmi):
# vbios_version
  properties = ['pid', 'name', 'gpu_name', 'gpu_uuid', 'used_memory']

  class ComputeApp:
    def __init__(self, pid, name, gpu_name, gpu_uuid, mem_used) -> None:
      self.pid = pid
      self.name = name
      self.gpu_name = gpu_name
      self.gpu_uuid = gpu_uuid
      self.mem_used = mem_used

    def attribute_list(self):
      return [self.pid, self.name, self.gpu_name, self.gpu_uuid, self.mem_used]

  def __init__(self) -> None:
    super().__init__()
    self.query_switch = 'query-compute-apps'

  def query_compute_apps(self):
    apps = []
    nv_smi = self.query(ComputeAppQuery.properties)
    for i in nv_smi.stdout:
      cols = i.strip().split(',')
      apps.append(ComputeAppQuery.ComputeApp(int(cols[0]), cols[1], cols[2], cols[3], int(cols[4])))
    nv_smi.stdout.close()
    return apps

  def list_compute_apps(self):
    apps = self.query_compute_apps()
    if apps:
      print(self.ors.join(ComputeAppQuery.properties))
      for i in apps:
        print(self.ors.join(str(x) for x in i.attribute_list()))
