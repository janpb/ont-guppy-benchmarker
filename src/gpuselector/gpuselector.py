"""
..
  Copyright 2022 Jan P Buchmann

.. moduleauthor:: Jan Piotr Buchmann <jpb@members.fsf.org>
"""

import os
import sys

import nvidiasmi.gpuquery

class GpuSelector:
  header = ['#gpuIdx', 'gpuUuid', 'gpuName', 'gpuFreeMemory[b]', 'gpuTotalMemory[b]',
            'gpuAvailMemory[b]',  'gpuDriverVersion']

  def __init__(self, gpu_mem_scaling_factor=1) -> None:
    self.gpus = {}
    self.gpu_uuid = None
    self.gpu_mem_scaling_factor = gpu_mem_scaling_factor

  def select_gpus_by_uuid(self, gpu_uuids=None):
    gpus = nvidiasmi.gpuquery.GpuQuery().query_gpus()
    if gpu_uuids is None and gpus:
      self.gpu_uuid = next(iter(gpus.items()))[1].uuid
      self.gpus[self.gpu_uuid] =  gpus.pop(self.gpu_uuid)
      return self.gpu_list()

    limiting_mem = sys.maxsize
    for i in gpu_uuids:
      gpu = gpus.get(i, None)
      if gpu:
        if self.memory_available(gpu) < limiting_mem:
          limiting_mem = self.memory_available(gpu)
          self.gpu_uuid = gpu.uuid
          self.gpus[self.gpu_uuid] =  gpus.pop(self.gpu_uuid)
      else:
        print(f"GPU uuid not found: {i}", file=sys.stderr)
    if not self.gpus:
      return []
    return self.gpu_list()

  def gpu_by_uuid(self, uuid):
    return self.gpus.get(uuid, None)

  def gpu_list(self):
    return [self.gpus[x] for x in self.gpus]

  def gpu(self):
    return self.gpu_by_uuid(self.gpu_uuid)

  def gpu_indices(self):
    return sorted([self.gpus[x].index for x in self.gpus])

  def write_log(self, benchmarkdir):
    fh = open(os.path.join(benchmarkdir, 'gpu.log'), 'w')
    fh.write("\t".join(GpuSelector.header)+"\n")
    for i in self.gpus:
      fh.write("\t".join(str(x) for x in [self.gpus[i].index, self.gpus[i].uuid,
        self.gpus[i].name, self.gpus[i].mem_free, self.gpus[i].mem_total,
        self.gpus[i].memory_avail(), self.gpus[i].driver_version]) + "\n")
    fh.close()

  def has_enough_gpu_memory(self, required):
    return required < self.memory_available()

  def memory_available(self, gpu=None):
    if gpu:
      return gpu.free_memory() * self.gpu_mem_scaling_factor
    return self.gpu().free_memory() * self.gpu_mem_scaling_factor

  def list_gpu_uuids(self):
    return sorted([x.uuid for x in self.gpu_list()])
