#!/usr/bin/env python3
"""
..
  Copyright 2022 Jan P Buchmann

.. moduleauthor:: Jan Piotr Buchmann <jpb@members.fsf.org>
"""

import argparse
import hashlib
import os
import sys

import benchmark.settings
import benchmark.resultfile
import gpuselector.gpuselector
import guppy.guppy
import guppy.resultparser
import nvidiasmi.computeappquery
import nvidiasmi.gpuquery
import nvidiasmi.monitor


class GuppyBenchmarker:

  def __init__(self, gpu_selector) -> None:
    self.runs = []
    self.settings = None
    self.gpu_selector = gpu_selector
    self.resultfile = None

  def prepare(self, args):
    self.prepare_gpus(args.gpu_uuids)
    self.settings = benchmark.settings.BenchmarkSettings(args, self.gpu_selector.memory_available())
    self.resultfile = benchmark.resultfile.ResultFile(os.path.join(self.settings.output_dir, args.results))
    self.check_running_compute_apps(args.skip_apps_check)
    self.prepare_runs(args.dry_run)
    self.make_dirs(self.settings.output_dir)


  def calculate_uid(self, parameters):
    h = hashlib.md5()
    for i in parameters:
      h.update(str(i).encode())
    return h.hexdigest()

  def run(self):
    self.resultfile.open()
    self.resultfile.write_header(benchmark.resultfile.ResultFile.columns + guppy.resultparser.GuppyResultParser.result_columns)
    bmark_count = 0
    for i in self.runs:
      bmark_count += 1
      print(f"Benchmark {bmark_count} / {len(self.runs)} ({bmark_count/len(self.runs):3f}): {' '.join([str(x) for x in i.assemble_command()])}")
      run_params = i.run_parameters()
      self.make_dirs(i.save_path())
      results = i.run()
      self.resultfile.write_run_parameters(run_params, benchmark.resultfile.ResultFile.columns)
      self.resultfile.write_ors()
      self.resultfile.write_results(results, guppy.resultparser.GuppyResultParser.result_columns)
      self.resultfile.write_newline()
    self.resultfile.close()

  def make_dirs(self, directory):
    if not os.path.exists(directory):
      os.makedirs(directory)

  def guppy_save_path(self, ruid):
    return os.path.join(self.settings.output_dir, 'benchmark-'+ruid, 'guppy-run')

  def prepare_runs(self, do_dry_run):
    for i in self.settings.list_guppy_configs():
      def_ruid = "def-"+self.calculate_uid(self.gpu_selector.list_gpu_uuids() + [i])
      def_gpy = guppy.guppy.Guppy(def_ruid, self.settings.guppy_path, self.settings.guppy_version, i, self.settings.memory_available/2**20, self.settings.output_dir)
      def_gpy.set_gpu_device(self.gpu_selector.gpu_indices())
      def_gpy.set_input_path(self.settings.input_dir)
      self.runs.append(def_gpy)
      for j in self.settings.list_chunk_sizes():
        for k in self.settings.list_chunks_per_caller():
          for l in self.settings.list_gpu_runners():
            cpr = self.calculate_chunks_pre_runner(i, j, l)
            ruid = self.calculate_uid(self.gpu_selector.list_gpu_uuids() + [j, k, l, cpr, i.name()])
            gpy = guppy.guppy.Guppy(ruid, self.settings.guppy_path, self.settings.guppy_version, i, self.settings.memory_available/2**20, self.settings.output_dir)
            gpy.set_chunk_size(j)
            gpy.set_chunks_per_caller(k)
            gpy.set_gpu_runners_per_device(l)
            gpy.set_chunks_per_runner(cpr)
            gpy.set_input_path(self.settings.input_dir)
            gpy.set_gpu_device(self.gpu_selector.gpu_indices())
            if self.gpu_selector.has_enough_gpu_memory(gpy.required_memory()):
              self.runs.append(gpy)
            else:
              print(f"Benchmark {gpy.ruid}: GPU: {self.gpu_selector.gpu().uuid}: not enough memory. \
                      Required: {gpy.required_memory()}, avail:{self.settings.memory_available}", file=sys.stderr)
    print(f"Running {len(self.runs)} benchmarks on {self.settings.memory_available/2**20} Mib GPU memory", file=sys.stderr)
    if do_dry_run:
      print("Dry run. Following benchmarks would be run:", file=sys.stderr)
      for i in self.runs:
        i.show_command()
      sys.exit("Dry run options set. Not continuing.")

  def check_running_compute_apps(self, skip):
    nvi_smi = nvidiasmi.computeappquery.ComputeAppQuery()
    apps = nvi_smi.query_compute_apps()
    if apps:
      print("Following processes use GPUs:", file=sys.stderr)
      nvi_smi.list_compute_apps()
      if not skip:
        sys.exit("Either stop them or rerun the benchmark with the --skip-apps-check flag. Exiting")
      print("\t--skip-apps-check set. Continuing with benchmark.", file=sys.stderr)
      self.settings.use_tot_mem = False
    else:
      print("No processes using GPUs found.", file=sys.stderr)

  def prepare_gpus(self, gpu_uuids):
    gpus = self.gpu_selector.select_gpus_by_uuid(gpu_uuids)
    if len(gpus) == 0:
      sys.exit("Could not find GPUs. Check with --list-gpus. Abort.")
    print(f"Found {len(gpus)} GPU(s). GPU selected to set configure benchmark:", file=sys.stderr)
    gpu_attribs = self.gpu_selector.gpu().attributes()
    for i in gpu_attribs:
      print(f"\t{i}: {gpu_attribs[i]}",file=sys.stderr)

  def max_cpr(self, gpu_memory, modelfactor, chunk_size):
    return gpu_memory / (chunk_size * modelfactor)

  def calculate_chunks_pre_runner(self, guppy_config, chunk_size, runners):
    max_cpr = self.max_cpr(self.gpu_selector.memory_available(), guppy_config.model_factor(), chunk_size)
    return (max_cpr / runners) - ((max_cpr / runners) % 2 )

def main():
  ap = argparse.ArgumentParser(description='ONT gupy benchmark')
  ap.add_argument('--guppy', help='Path to guppy binary')
  ap.add_argument('--indir', help='Path to directory with fast5 files for benchmark')
  ap.add_argument('--outdir', help='Path to store benchmark results', default=os.getcwd())
  ap.add_argument('--cfg', nargs='+', help='guppy config files for benchmarking')
  ap.add_argument('--gpu-uuids', help='GPU uuids. Check --list-gpus to get uuids. Select first found GPU if non specified', nargs='+', default=None)
  ap.add_argument('--list-gpus', help='List available GPUs', action='store_true', default=False)
  ap.add_argument('--mem-scaling-factor', help='Factor to reduce available memory for calculations.', type=float, default=0.92)
  ap.add_argument('--skip-apps-check', help='Continue benchmark if othe processes use GPUs for computing', action='store_true', default=False)
  ap.add_argument('--results', help='Path to file storing results', type=str, default='guppy-benchmark.tsv')
  ap.add_argument('--dry-run', help='Show the planned benchmarks withut running them', action='store_true', default=False)
  args = ap.parse_args()

  if args.list_gpus:
    nvidiasmi.gpuquery.GpuQuery().list_gpus()
    sys.exit()
  gpb = GuppyBenchmarker(gpuselector.gpuselector.GpuSelector(args.mem_scaling_factor))
  gpb.prepare(args)
  gpb.run()
  # ../src/guppy-benchmark.py  --cfg /opt/ont/guppy/data/dna_r10.4_e8.1_fast.cfg  /opt/ont/guppy/data/dna_r9.4.1_450bps_hac.cfg --indir ../data/example_fast5 --guppy /opt/ont/guppy-6.0.1/bin/guppy_basecaller --dry-run
  return 0

if __name__ == '__main__':
  main()
