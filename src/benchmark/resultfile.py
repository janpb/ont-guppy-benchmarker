"""
..
  Copyright 2022 Jan P Buchmann

.. moduleauthor:: Jan Piotr Buchmann <jpb@members.fsf.org>
"""

class ResultFile:

  columns = ['ruid', 'path', 'version', 'config', 'gpu_runners_per_device',
             'chunk_size', 'chunks_per_runner', 'chunks_per_caller']
  ors = "\t"
  def __init__(self, path, sep="\t") -> None:
    self.path = path
    self.fh = None
    ResultFile.ors = sep

  def open(self):
    self.fh = open(self.path, 'w')
    return self.fh

  def write_run_parameters(self, parameters, columns):
    cols = []
    for i in columns:
      if i in parameters:
        cols.append(str(parameters[i]))
      else:
        cols.append('none')
    self.write(ResultFile.ors.join(str(x) for x in cols))

  def write_results(self, results, columns):
    cols = []
    for i in columns:
      if i in results:
        cols.append(str(results[i]))
      else:
        cols.append('none')
    self.write(ResultFile.ors.join(cols))

  def write(self, value):
    self.fh.write(value)

  def write_ors(self):
    self.write(ResultFile.ors)

  def write_newline(self):
    self.write("\n")

  def close(self):
    self.fh.close()

  def write_header(self, columns):
    self.fh.write('#' + ResultFile.ors.join(columns)+"\n")
