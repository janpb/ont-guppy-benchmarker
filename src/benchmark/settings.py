"""
..
  Copyright 2022 Jan P Buchmann

.. moduleauthor:: Jan Piotr Buchmann <jpb@members.fsf.org>
"""

from ensurepip import version
import os

import guppy.config
import guppy.guppy

class BenchmarkSettings:

  class ParameterChunkSize:

    def __init__(self, exp_min=7, exp_max=13) -> None:
      self.min = exp_min
      self.max = exp_max

    def chunk_sizes(self):
      return [2**x for x in range(self.min, self.max)]

  class ParameterChunksPerCaller:

    def __init__(self, min=5000, max=20000, step=5000) -> None:
      self.min = min
      self.max = max
      self.step = step

    def chunks_per_caller(self):
      return [x for x in range(self.min, self.max, self.step)]

  class ParameterGpuRunners:

    def __init__(self, min=8, max=16, step=4) -> None:
      self.min = min
      self.max = max+1
      self.step = step

    def gpu_runners(self):
      return [x for x in range(self.min, self.max, self.step)]

  def __init__(self, args, memory_available) -> None:
    self.input_dir = args.indir
    self.output_dir = os.path.join(args.outdir, 'guppy-benchmark')
    self.guppy_path = args.guppy
    self.chunk_size_paramters = BenchmarkSettings.ParameterChunkSize()
    self.chunks_per_caller_parameters = BenchmarkSettings.ParameterChunksPerCaller()
    self.gpu_runners = BenchmarkSettings.ParameterGpuRunners()
    self.guppy_configs = self.add_guppy_configs(args.cfg)
    self.memory_available = memory_available
    self.guppy_version = 0
    self.mmap_version = 0
    self.find_version(args.guppy)

  def find_version(self, guppypath):
    versions = guppy.guppy.Guppy.guppy_version(guppypath)
    self.guppy_version = versions.get('guppyver', 0)
    self.mmap_version = versions.get('mmapver', 0)

  def add_guppy_configs(self, guppy_configs):
    seen = set()
    configs = []
    for i in guppy_configs:
      cfg = guppy.config.GuppyConfig().configure(i)
      if cfg.name not in seen:
        configs.append(cfg)
        seen.add(cfg.name)
    return configs

  def list_guppy_configs(self):
    return self.guppy_configs

  def list_chunk_sizes(self):
    return self.chunk_size_paramters.chunk_sizes()

  def list_chunks_per_caller(self):
    return self.chunks_per_caller_parameters.chunks_per_caller()

  def list_gpu_runners(self):
    return self.gpu_runners.gpu_runners()
